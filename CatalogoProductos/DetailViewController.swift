//
//  DetailViewController.swift
//  CatalogoProductos
//
//  Created by AlexM on 29/01/15.
//  Copyright (c) 2015 AlexM. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!

    @IBOutlet weak var titleProduct: UILabel!
    @IBOutlet weak var priceProduct: UILabel!

    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if (arrayProduct.count > 0) {
            if let detail: Product = self.detailItem as? Product {
                if let label = self.titleProduct {
                    label.text = detail.title
                }
                
                if let label = self.priceProduct {
                    label.text = String(detail.price)
                }
            }
        }
        
        else {
            if let label = self.titleProduct {
                label.text = ""
            }
            
            if let label = self.priceProduct {
                label.text = ""
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

