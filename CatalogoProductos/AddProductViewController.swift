//
//  AddProductViewController.swift
//  CatalogoProductos
//
//  Created by AlexM on 29/01/15.
//  Copyright (c) 2015 AlexM. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController {
    @IBOutlet weak var titleProduct: UITextField!
    @IBOutlet weak var priceProduct: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addProduct(sender: AnyObject, forEvent event: UIEvent) {
        if ((priceProduct.text.toInt()) != nil) {
            var p = Product(title: titleProduct.text, price: priceProduct.text.toInt()!)
            arrayProduct.append(p)
        }
        
        else {
            let alertController = UIAlertController(title: "Invalid Number", message:"The price has to be a Number", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
        
        NSNotificationCenter.defaultCenter().postNotificationName("reloadProducts", object: nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
