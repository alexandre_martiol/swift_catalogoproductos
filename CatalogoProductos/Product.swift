//
//  Product.swift
//  CatalogoProductos
//
//  Created by AlexM on 29/01/15.
//  Copyright (c) 2015 AlexM. All rights reserved.
//

import UIKit

var arrayProduct: [Product] = Array()

class Product: NSObject {
    var title = ""
    var price = 0
    
    init(title:String, price:NSInteger) {
        self.title = title
        self.price = price
    }
}
